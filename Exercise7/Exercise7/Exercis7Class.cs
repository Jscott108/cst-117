﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise7
{
    class Exercis7Class
    {


        public void Main(string[] args)
        {
            showMeters(58752);
        }

        public decimal showMeters(int numMillimeters)
        {
            decimal Meters;
            decimal Millimeters = Convert.ToDecimal(numMillimeters);
            Meters = Millimeters / 1000;
            return Meters;
        }

        public int showSum(int Num1, int Num2)
        {
            int Sum = Num1 + Num2;
            return Sum;
        }

        public double doubleAvg(double num1, double num2, double num3, double num4, double num5)
        {
            List<double> nums = new List<double>();
            nums.Add(num1);
            nums.Add(num2);
            nums.Add(num3);
            nums.Add(num4);
            nums.Add(num5);

            double avg = nums.Average();
            return avg;
        }

        public int rngSum(int r1, int r2)
        {
            Random rand = new Random();
            int rand1 = rand.Next(1, r1);
            int rand2 = rand.Next(1, r2);

            int randSum = rand1 + rand2;

            return randSum;
        }

        public Boolean difThree(int num1, int num2, int num3)
        {
            Boolean divBy3;
            int numDiv = num1 + num2 + num3;
            if (numDiv % 3 == 0)
            {
                divBy3 = true;
            }
            else
            {
                divBy3 = false;
            }

            return divBy3;
        }

        public void getShortest(string word1, string word2)
        {
            string[] words = { word1, word2 };
            IEnumerable<String> query = from word in words
                                        orderby word.Length, word.Substring(0,1)
                                        select word;
            string shortest = words.First();
            Console.WriteLine(shortest);
        }

        public double getLargestDoub(double[] nums)
        {
            double largest = nums.Max();
            return largest;
        }

        public int[] GenerateArray()
        {
            int i = 0;
            Random rand = new Random();
            int[] NumArray = new int[0];
            while (i <= 50)
            {
                int nextNum = rand.Next();
                NumArray.Append(nextNum);
            }
            return NumArray;
        }

        public Boolean checkBool(Boolean bool1, Boolean bool2)
        {

            Boolean match;
            if(bool1 == bool2)
            {
                match = true;
            }
            else
            {
                match = false;
            }

            return match;
        }

        public double returnProd(int num1, double num2)
        {
            double prod = num1 * num2;
            return prod;
        }

        public int twoDavg(int[,] array)
        {
            int sum = array.Cast<int>().Sum();
            int avg = sum / array.Length;
            return avg;
        }


    }
}
