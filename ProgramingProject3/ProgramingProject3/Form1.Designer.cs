﻿namespace ProgramingProject3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TXB_FilePath = new System.Windows.Forms.TextBox();
            this.BTN_Browse = new System.Windows.Forms.Button();
            this.TXB_Results = new System.Windows.Forms.RichTextBox();
            this.BTN_Run = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // TXB_FilePath
            // 
            this.TXB_FilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_FilePath.Location = new System.Drawing.Point(22, 12);
            this.TXB_FilePath.Name = "TXB_FilePath";
            this.TXB_FilePath.ReadOnly = true;
            this.TXB_FilePath.Size = new System.Drawing.Size(392, 26);
            this.TXB_FilePath.TabIndex = 0;
            // 
            // BTN_Browse
            // 
            this.BTN_Browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Browse.Location = new System.Drawing.Point(420, 13);
            this.BTN_Browse.Name = "BTN_Browse";
            this.BTN_Browse.Size = new System.Drawing.Size(78, 24);
            this.BTN_Browse.TabIndex = 1;
            this.BTN_Browse.Text = "Browse";
            this.BTN_Browse.UseVisualStyleBackColor = true;
            this.BTN_Browse.Click += new System.EventHandler(this.button1_Click);
            // 
            // TXB_Results
            // 
            this.TXB_Results.Location = new System.Drawing.Point(22, 80);
            this.TXB_Results.Name = "TXB_Results";
            this.TXB_Results.ReadOnly = true;
            this.TXB_Results.Size = new System.Drawing.Size(476, 262);
            this.TXB_Results.TabIndex = 2;
            this.TXB_Results.Text = "";
            // 
            // BTN_Run
            // 
            this.BTN_Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Run.Location = new System.Drawing.Point(215, 44);
            this.BTN_Run.Name = "BTN_Run";
            this.BTN_Run.Size = new System.Drawing.Size(84, 30);
            this.BTN_Run.TabIndex = 3;
            this.BTN_Run.Text = "Run";
            this.BTN_Run.UseVisualStyleBackColor = true;
            this.BTN_Run.Click += new System.EventHandler(this.BTN_Run_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 365);
            this.Controls.Add(this.BTN_Run);
            this.Controls.Add(this.TXB_Results);
            this.Controls.Add(this.BTN_Browse);
            this.Controls.Add(this.TXB_FilePath);
            this.Name = "Form1";
            this.Text = "Project 3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox TXB_FilePath;
        private System.Windows.Forms.Button BTN_Browse;
        private System.Windows.Forms.RichTextBox TXB_Results;
        private System.Windows.Forms.Button BTN_Run;
    }
}

