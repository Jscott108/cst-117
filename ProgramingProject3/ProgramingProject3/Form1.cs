﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramingProject3
{
    public partial class Form1 : Form
    {
        string filePath;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse for Text File",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "txt",
                Filter = "txt files (*.txt)|*.txt",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true,
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                TXB_FilePath.Text = openFileDialog.FileName;
                filePath = openFileDialog.FileName;
            }
        }

        private void BTN_Run_Click(object sender, EventArgs e)
        {
            //get the base text
            string text = System.IO.File.ReadAllText(filePath);
            
            //get the lower case version
            string lower = text.ToLower();

            //building an array for next steps
            string[] words = lower.Split(new[] { " " }, StringSplitOptions.None);
            List<string> wordList = words.ToList<string>();

            //find the first and last word a-z
            string firstWord = "";
            string lastWord = "";
            wordList.Sort();





            foreach(String s in words)
            {

            }

            //find the longest word
            string lngword = "";
            int ctl = 0;
            foreach(String s in words)
            {
                if (s.Length > ctl)
                {
                    lngword = s;
                    ctl = s.Length;
                }
            }

            TXB_Results.Text = text + lngword;
        }
    }
}
