﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConversionCalculator
{
    public partial class MMtoINconversion : Form
    {
        public MMtoINconversion()
        {
            InitializeComponent();
        }

        private void BTN_Convert_click(object sender, EventArgs e)
        {
            Double Millimeters = Convert.ToDouble(TXB_Millimeters.Text);
            Double Centemeters = Millimeters / 10;
            Double Inches = Centemeters / 2.54;
            TXB_Inches.Text = Convert.ToString(Inches);
        }

        private void TXB_Millimeters_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
