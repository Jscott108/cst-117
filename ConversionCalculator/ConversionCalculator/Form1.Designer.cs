﻿namespace ConversionCalculator
{
    partial class MMtoINconversion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TXB_Millimeters = new System.Windows.Forms.TextBox();
            this.TXB_Inches = new System.Windows.Forms.TextBox();
            this.BTN_Convert = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Legnth in Millimeters";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Length in Inches";
            // 
            // TXB_Millimeters
            // 
            this.TXB_Millimeters.Location = new System.Drawing.Point(229, 58);
            this.TXB_Millimeters.Name = "TXB_Millimeters";
            this.TXB_Millimeters.Size = new System.Drawing.Size(100, 20);
            this.TXB_Millimeters.TabIndex = 2;
            this.TXB_Millimeters.Text = "0";
            this.TXB_Millimeters.TextChanged += new System.EventHandler(this.TXB_Millimeters_TextChanged);
            // 
            // TXB_Inches
            // 
            this.TXB_Inches.Location = new System.Drawing.Point(229, 115);
            this.TXB_Inches.Name = "TXB_Inches";
            this.TXB_Inches.Size = new System.Drawing.Size(100, 20);
            this.TXB_Inches.TabIndex = 3;
            this.TXB_Inches.Text = "0";
            // 
            // BTN_Convert
            // 
            this.BTN_Convert.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Convert.Location = new System.Drawing.Point(239, 165);
            this.BTN_Convert.Name = "BTN_Convert";
            this.BTN_Convert.Size = new System.Drawing.Size(75, 23);
            this.BTN_Convert.TabIndex = 4;
            this.BTN_Convert.Text = "Convert";
            this.BTN_Convert.UseVisualStyleBackColor = true;
            this.BTN_Convert.Click += new System.EventHandler(this.BTN_Convert_click);
            // 
            // MMtoINconversion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 220);
            this.Controls.Add(this.BTN_Convert);
            this.Controls.Add(this.TXB_Inches);
            this.Controls.Add(this.TXB_Millimeters);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MMtoINconversion";
            this.Text = "MM to IN convertion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TXB_Millimeters;
        private System.Windows.Forms.TextBox TXB_Inches;
        private System.Windows.Forms.Button BTN_Convert;
    }
}

