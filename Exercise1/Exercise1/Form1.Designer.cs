﻿namespace Exercise1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_AddConsole = new System.Windows.Forms.Button();
            this.SCRLBR_Main = new System.Windows.Forms.VScrollBar();
            this.TXTB_ConsoleName = new System.Windows.Forms.TextBox();
            this.LSTB_Consoles = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // BTN_AddConsole
            // 
            this.BTN_AddConsole.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.BTN_AddConsole.Location = new System.Drawing.Point(326, 42);
            this.BTN_AddConsole.Name = "BTN_AddConsole";
            this.BTN_AddConsole.Size = new System.Drawing.Size(75, 27);
            this.BTN_AddConsole.TabIndex = 0;
            this.BTN_AddConsole.Text = "Add Console";
            this.BTN_AddConsole.UseVisualStyleBackColor = true;
            // 
            // SCRLBR_Main
            // 
            this.SCRLBR_Main.Location = new System.Drawing.Point(780, 1);
            this.SCRLBR_Main.Name = "SCRLBR_Main";
            this.SCRLBR_Main.Size = new System.Drawing.Size(19, 449);
            this.SCRLBR_Main.TabIndex = 1;
            // 
            // TXTB_ConsoleName
            // 
            this.TXTB_ConsoleName.Location = new System.Drawing.Point(421, 46);
            this.TXTB_ConsoleName.Name = "TXTB_ConsoleName";
            this.TXTB_ConsoleName.Size = new System.Drawing.Size(117, 20);
            this.TXTB_ConsoleName.TabIndex = 2;
            this.TXTB_ConsoleName.Text = "Enter Console Name";
            this.TXTB_ConsoleName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // LSTB_Consoles
            // 
            this.LSTB_Consoles.FormattingEnabled = true;
            this.LSTB_Consoles.Location = new System.Drawing.Point(326, 87);
            this.LSTB_Consoles.Name = "LSTB_Consoles";
            this.LSTB_Consoles.Size = new System.Drawing.Size(120, 95);
            this.LSTB_Consoles.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LSTB_Consoles);
            this.Controls.Add(this.TXTB_ConsoleName);
            this.Controls.Add(this.SCRLBR_Main);
            this.Controls.Add(this.BTN_AddConsole);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTN_AddConsole;
        private System.Windows.Forms.VScrollBar SCRLBR_Main;
        private System.Windows.Forms.TextBox TXTB_ConsoleName;
        private System.Windows.Forms.ListBox LSTB_Consoles;
    }
}

