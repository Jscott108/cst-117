﻿namespace Primordial_ISCv2
{
    partial class AddProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.ListView1 = new System.Windows.Forms.ListView();
            this.TXB_Tags = new System.Windows.Forms.TextBox();
            this.TXB_Quantaty = new System.Windows.Forms.TextBox();
            this.TXB_Cost = new System.Windows.Forms.TextBox();
            this.TXB_Cat = new System.Windows.Forms.TextBox();
            this.TXB_Desc = new System.Windows.Forms.TextBox();
            this.TXB_Name = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.LBL_Name = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Button2
            // 
            this.Button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button2.Location = new System.Drawing.Point(634, 103);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(111, 52);
            this.Button2.TabIndex = 31;
            this.Button2.Text = "Cancel";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Button1
            // 
            this.Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.Location = new System.Drawing.Point(634, 36);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(111, 52);
            this.Button1.TabIndex = 30;
            this.Button1.Text = "Add";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // ListView1
            // 
            this.ListView1.HideSelection = false;
            this.ListView1.Location = new System.Drawing.Point(191, 223);
            this.ListView1.Name = "ListView1";
            this.ListView1.Size = new System.Drawing.Size(381, 191);
            this.ListView1.TabIndex = 29;
            this.ListView1.UseCompatibleStateImageBehavior = false;
            // 
            // TXB_Tags
            // 
            this.TXB_Tags.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Tags.Location = new System.Drawing.Point(191, 191);
            this.TXB_Tags.Name = "TXB_Tags";
            this.TXB_Tags.Size = new System.Drawing.Size(381, 26);
            this.TXB_Tags.TabIndex = 28;
            // 
            // TXB_Quantaty
            // 
            this.TXB_Quantaty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Quantaty.Location = new System.Drawing.Point(191, 159);
            this.TXB_Quantaty.Name = "TXB_Quantaty";
            this.TXB_Quantaty.Size = new System.Drawing.Size(381, 26);
            this.TXB_Quantaty.TabIndex = 27;
            // 
            // TXB_Cost
            // 
            this.TXB_Cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Cost.Location = new System.Drawing.Point(191, 129);
            this.TXB_Cost.Name = "TXB_Cost";
            this.TXB_Cost.Size = new System.Drawing.Size(381, 26);
            this.TXB_Cost.TabIndex = 26;
            // 
            // TXB_Cat
            // 
            this.TXB_Cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Cat.Location = new System.Drawing.Point(191, 97);
            this.TXB_Cat.Name = "TXB_Cat";
            this.TXB_Cat.Size = new System.Drawing.Size(381, 26);
            this.TXB_Cat.TabIndex = 25;
            // 
            // TXB_Desc
            // 
            this.TXB_Desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Desc.Location = new System.Drawing.Point(191, 68);
            this.TXB_Desc.Name = "TXB_Desc";
            this.TXB_Desc.Size = new System.Drawing.Size(381, 26);
            this.TXB_Desc.TabIndex = 24;
            // 
            // TXB_Name
            // 
            this.TXB_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Name.Location = new System.Drawing.Point(191, 36);
            this.TXB_Name.Name = "TXB_Name";
            this.TXB_Name.Size = new System.Drawing.Size(381, 26);
            this.TXB_Name.TabIndex = 23;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(132, 194);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(53, 20);
            this.Label6.TabIndex = 22;
            this.Label6.Text = "Tags:";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(104, 162);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(81, 20);
            this.Label5.TabIndex = 21;
            this.Label5.Text = "Quantity:";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(134, 132);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(51, 20);
            this.Label4.TabIndex = 20;
            this.Label4.Text = "Cost:";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(112, 221);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(73, 20);
            this.Label3.TabIndex = 19;
            this.Label3.Text = "Images:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(56, 100);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(129, 20);
            this.Label2.TabIndex = 18;
            this.Label2.Text = "Type/Catagory:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(80, 68);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(105, 20);
            this.Label1.TabIndex = 17;
            this.Label1.Text = "Description:";
            // 
            // LBL_Name
            // 
            this.LBL_Name.AutoSize = true;
            this.LBL_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Name.Location = new System.Drawing.Point(125, 39);
            this.LBL_Name.Name = "LBL_Name";
            this.LBL_Name.Size = new System.Drawing.Size(60, 20);
            this.LBL_Name.TabIndex = 16;
            this.LBL_Name.Text = "Name:";
            // 
            // AddProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.ListView1);
            this.Controls.Add(this.TXB_Tags);
            this.Controls.Add(this.TXB_Quantaty);
            this.Controls.Add(this.TXB_Cost);
            this.Controls.Add(this.TXB_Cat);
            this.Controls.Add(this.TXB_Desc);
            this.Controls.Add(this.TXB_Name);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.LBL_Name);
            this.Name = "AddProduct";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.ListView ListView1;
        internal System.Windows.Forms.TextBox TXB_Tags;
        internal System.Windows.Forms.TextBox TXB_Quantaty;
        internal System.Windows.Forms.TextBox TXB_Cost;
        internal System.Windows.Forms.TextBox TXB_Cat;
        internal System.Windows.Forms.TextBox TXB_Desc;
        internal System.Windows.Forms.TextBox TXB_Name;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label LBL_Name;
    }
}

