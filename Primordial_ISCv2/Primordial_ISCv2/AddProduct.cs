﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Primordial_ISCv2.PrimordialDirver;


namespace Primordial_ISCv2
{
    public partial class AddProduct : Form
    {
        public AddProduct()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Product inventoryitem = new Product();
            inventoryitem.name = Convert.ToString(TXB_Name.Text);
            inventoryitem.description = Convert.ToString(TXB_Desc.Text);
            inventoryitem.cost = Convert.ToDouble(TXB_Cost.Text);
            inventoryitem.stock = Convert.ToInt32(TXB_Quantaty.Text);
            inventoryitem.type = Convert.ToString(TXB_Cat.Text);

            addNewInventory(inventoryitem);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            PrimordialDirver.windowToShow = PrimordialDirver.windowShown.dashboard;
            PrimordialDirver.ChangeWindow();
            this.Hide();
        }
    }
}
