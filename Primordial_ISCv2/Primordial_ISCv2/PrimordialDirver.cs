﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static Primordial_ISCv2.Inventory;


//Main Driver system
namespace Primordial_ISCv2
{
    class PrimordialDirver
    {
        public enum windowShown { dashboard, addProduct, editProduct, removeProduct, search, viewHistory, login }
        enum lastWindow { dashboard, addProduct, editProduct, removeProduct, search, viewHistory, login }

        public static windowShown windowToShow;

        private static Dashboard frmDashboard = new Dashboard();
        private static AddProduct frmAddProduct = new AddProduct();
        private static EditProduct frmEditProduct = new EditProduct();
        private static RemoveProduct frmRemoveProduct = new RemoveProduct();
        private static Search frmSearch = new Search();
        private static ViewAll frmViewHistory = new ViewAll();
        public static XDocument doc = new XDocument();
        public static Inventory inventory = new Inventory();

        public void PrimordialDriver()
        {

        }

        public void LoginConfirmed(string Username, string Password)
        {
            windowToShow = windowShown.dashboard;
            LoginScreen.ActiveForm.Hide();
            ChangeWindow();
        }


        //Drive function to add a new inventory item
        public static void addNewInventory(Product item)
        {
            if (inventory.doesEntryExist(item))
                Console.WriteLine("Item already exists, please use Edit Inventory");
            else
            {
                inventory.addProduct(item);
                Console.WriteLine("Item added to Inventory");
            }
         }


        //Drive function to remove an inventory item
        public void removeInventory(Product item, String items)
        {
            if (inventory.doesEntryExist(item))
                inventory.DeleteInventoryItem(items);
            else
            {
                Console.WriteLine("No Items Found");
            }
        }


        //drive dunction to restock item
        public void restockInventory(Product item)
        {
            if (inventory.doesEntryExist(item))
                Console.WriteLine("Item already exists, please use Edit Inventory");
            else
            {
                inventory.addProduct(item);
                Console.WriteLine("Item added to Inventory");
            }
        }


        //Drive function to display all items
        public void displayInventory()
        {
            inventory.printAllItems();
        }


        //Drive Dunction to search items

        public static void ChangeWindow()
        {

            switch (windowToShow)
            {
                case windowShown.dashboard:
                    frmDashboard.Show();
                    break;
                case windowShown.addProduct:
                    frmAddProduct.Show();
                    break;
                case windowShown.editProduct:
                    frmEditProduct.Show();
                    break;
                case windowShown.removeProduct:
                    frmRemoveProduct.Show();
                    break;
                case windowShown.search:
                    frmSearch.Show();
                    break;
                case windowShown.viewHistory:
                    frmViewHistory.Show();
                    break;
                case windowShown.login:
                    break;
            }
        }

        private void OnExit(object sender, FormClosedEventArgs e)
        {

        }
    }
}
