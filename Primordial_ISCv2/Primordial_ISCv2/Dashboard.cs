﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Primordial_ISCv2
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
            

        }

        private void Button4_Click(object sender, EventArgs e)
        {
            PrimordialDirver.windowToShow = PrimordialDirver.windowShown.addProduct;
            PrimordialDirver.ChangeWindow();
            this.Hide();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            PrimordialDirver.windowToShow = PrimordialDirver.windowShown.viewHistory;
            PrimordialDirver.ChangeWindow();
            this.Hide();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            PrimordialDirver.windowToShow = PrimordialDirver.windowShown.search;
            PrimordialDirver.ChangeWindow();
            this.Hide();
        }

        private void Button3_Click(object sender, EventArgs e)
        {

        }

        private void Button5_Click(object sender, EventArgs e)
        {
            PrimordialDirver.windowToShow = PrimordialDirver.windowShown.removeProduct;
            PrimordialDirver.ChangeWindow();
            this.Hide();
        }
    }
}
