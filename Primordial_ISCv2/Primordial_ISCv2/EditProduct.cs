﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Primordial_ISCv2
{
    public partial class EditProduct : Form
    {
        public EditProduct()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            PrimordialDirver.windowToShow = PrimordialDirver.windowShown.dashboard;
            PrimordialDirver.ChangeWindow();
            this.Hide();
        }
    }
}
