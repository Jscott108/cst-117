﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Primordial_ISCv2
{
    [Serializable]
    class Product
    {
        public string name;
        public string description;
        public string type;
        public double cost;
        public int stock;
    }
}
