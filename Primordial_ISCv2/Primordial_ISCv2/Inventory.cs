﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.IO;
using System.ComponentModel.Design.Serialization;
using Primordial_ISCv2.Properties;
using System.Security;
using System.Xml;

namespace Primordial_ISCv2
{
    class Inventory
    {

        public static List<Product> inventoryArray = new List<Product>();


        void Inventroy()
        {

            //Get the saved inventory form the stored xml sheet.
            XElement SavedInventory = XElement.Load(@"E:\cst-117\Primordial_ISCv2\Primordial_ISCv2\Resources\Inventory.xml");
            inventoryArray = (from product in SavedInventory.Elements("product")
                              select new Product
                              {
                                  name = (string)product.Element("name"),
                                  description = (string)product.Element("description"),
                                  type = (string)product.Element("type"),
                                  stock = (int)product.Element("stock"),
                                  cost = (double)product.Element("cost")
                              })
                               .ToList();
        }

        public static void AddInventoryItem(Product item)
        {
            List<Product> inventoryList = new List<Product>();
            inventoryList.Add(item);
            System.Windows.Forms.MessageBox.Show(Convert.ToString(inventoryList));
        }


        //AddProduct product to the inventory
        public void addProduct(Product item)
        {
            inventoryArray.Add(item);

            //Update the XML with the new product
            DataContractSerializer serializer = new DataContractSerializer(typeof(Product));
            using (var stream = new FileStream(@"E:\cst-117\Primordial_ISCv2\Primordial_ISCv2\Resources\Inventory.xml", FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(stream))
            using (XmlTextWriter xmlWriter = new XmlTextWriter(sw))
            {
                xmlWriter.Formatting = Formatting.Indented;
                serializer.WriteObject(stream, item);
            }


        }


        //edit existing items in the inventory
        public void EditInventoryItem(Product item)
        {

        }

        //remove the item from the inventory
        public void DeleteInventoryItem(string item)
        {
            foreach(Product p in inventoryArray)
            {
                if (p.name.Equals(item))
                {
                    inventoryArray.Remove(p);
                    break;
                }
            }
        }


        //check if there is an entry matching the one in use
        public bool doesEntryExist(Product item)
        {
            bool bdoesExist = false;
            //we dont want to match everything, only the name as that is the main key
            foreach (Product p in inventoryArray)
                if (p.name.Equals(item.name))
                {
                    bdoesExist = true;
                    break;
                }
                else
                    bdoesExist = false;

            return bdoesExist;
        }


        //basic print function
        public void printAllItems()
        {
            foreach (Product p in inventoryArray)
            {
                Console.WriteLine(p.name);
            }
        }

        public IList<Product> search(string keyword, string catagory)
        {
            List<Product> products = new List<Product>();
            if (keyword == null)
            {
                foreach (Product p in Inventory.inventoryArray)
                    if (p.type.Contains(catagory))
                        products.Add(p);
                return products;
            }
            else if (catagory == null)
            {
                foreach (Product p in Inventory.inventoryArray)
                    if (p.name.Contains(keyword) && p.description.Contains(keyword))
                        products.Add(p);
                return products;
            }
            else if(keyword == null && catagory == null)
            {
                products = Inventory.inventoryArray;
            }
            else
            {
                foreach (Product p in Inventory.inventoryArray)
                    if (p.type.Contains(catagory) && p.name.Contains(keyword) && p.description.Contains(keyword))
                        products.Add(p);
            }

            return products;
        }
 
    }
}
