﻿namespace Primordial_ISCv2
{
    partial class LoginScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TXB_Password = new System.Windows.Forms.TextBox();
            this.TXB_Username = new System.Windows.Forms.TextBox();
            this.LBL_Password = new System.Windows.Forms.Label();
            this.LBL_Username = new System.Windows.Forms.Label();
            this.BTN_Login = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TXB_Password
            // 
            this.TXB_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Password.Location = new System.Drawing.Point(399, 210);
            this.TXB_Password.Name = "TXB_Password";
            this.TXB_Password.PasswordChar = '*';
            this.TXB_Password.Size = new System.Drawing.Size(214, 30);
            this.TXB_Password.TabIndex = 9;
            this.TXB_Password.Text = "Password";
            this.TXB_Password.UseSystemPasswordChar = true;
            // 
            // TXB_Username
            // 
            this.TXB_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Username.Location = new System.Drawing.Point(399, 171);
            this.TXB_Username.Name = "TXB_Username";
            this.TXB_Username.Size = new System.Drawing.Size(214, 30);
            this.TXB_Username.TabIndex = 8;
            this.TXB_Username.Text = "Username";
            // 
            // LBL_Password
            // 
            this.LBL_Password.AutoSize = true;
            this.LBL_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Password.Location = new System.Drawing.Point(267, 213);
            this.LBL_Password.Name = "LBL_Password";
            this.LBL_Password.Size = new System.Drawing.Size(104, 25);
            this.LBL_Password.TabIndex = 7;
            this.LBL_Password.Text = "Password:";
            // 
            // LBL_Username
            // 
            this.LBL_Username.AutoSize = true;
            this.LBL_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Username.Location = new System.Drawing.Point(188, 174);
            this.LBL_Username.Name = "LBL_Username";
            this.LBL_Username.Size = new System.Drawing.Size(183, 25);
            this.LBL_Username.TabIndex = 6;
            this.LBL_Username.Text = "Username or Email:";
            // 
            // BTN_Login
            // 
            this.BTN_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Login.Location = new System.Drawing.Point(512, 246);
            this.BTN_Login.Name = "BTN_Login";
            this.BTN_Login.Size = new System.Drawing.Size(101, 33);
            this.BTN_Login.TabIndex = 5;
            this.BTN_Login.Text = "Login";
            this.BTN_Login.UseVisualStyleBackColor = true;
            this.BTN_Login.Click += new System.EventHandler(this.BTN_Login_Click);
            // 
            // LoginScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TXB_Password);
            this.Controls.Add(this.TXB_Username);
            this.Controls.Add(this.LBL_Password);
            this.Controls.Add(this.LBL_Username);
            this.Controls.Add(this.BTN_Login);
            this.Name = "LoginScreen";
            this.Text = "LoginScreen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox TXB_Password;
        internal System.Windows.Forms.TextBox TXB_Username;
        internal System.Windows.Forms.Label LBL_Password;
        internal System.Windows.Forms.Label LBL_Username;
        internal System.Windows.Forms.Button BTN_Login;
    }
}