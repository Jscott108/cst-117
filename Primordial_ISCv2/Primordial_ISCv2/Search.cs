﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Primordial_ISCv2
{
    public partial class Search : Form
    {
        public Search()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PrimordialDirver.windowToShow = PrimordialDirver.windowShown.dashboard;
            PrimordialDirver.ChangeWindow();
            this.Hide();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string Keyword = Convert.ToString(TXB_Keyword);
            string Catagory = Convert.ToString(TXB_Catagory);
            foreach (Product p in PrimordialDirver.inventory.search(Convert.ToString(TXB_Keyword), Convert.ToString(TXB_Catagory)))
            {
                ListBox1.Items.Add(p.name.ToString());
            }
        }
    }
}
