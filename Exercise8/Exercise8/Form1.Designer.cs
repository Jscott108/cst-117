﻿namespace Exercise8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LBL_Fat = new System.Windows.Forms.Label();
            this.LBL_Cal = new System.Windows.Forms.Label();
            this.TXB_Fat = new System.Windows.Forms.TextBox();
            this.TXB_Cal = new System.Windows.Forms.TextBox();
            this.BTN_Fat = new System.Windows.Forms.Button();
            this.BTN_Cal = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LBL_Fat
            // 
            this.LBL_Fat.AutoSize = true;
            this.LBL_Fat.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Fat.Location = new System.Drawing.Point(62, 172);
            this.LBL_Fat.Name = "LBL_Fat";
            this.LBL_Fat.Size = new System.Drawing.Size(0, 22);
            this.LBL_Fat.TabIndex = 0;
            this.LBL_Fat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBL_Cal
            // 
            this.LBL_Cal.AutoSize = true;
            this.LBL_Cal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Cal.Location = new System.Drawing.Point(413, 172);
            this.LBL_Cal.Name = "LBL_Cal";
            this.LBL_Cal.Size = new System.Drawing.Size(0, 22);
            this.LBL_Cal.TabIndex = 1;
            this.LBL_Cal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TXB_Fat
            // 
            this.TXB_Fat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Fat.Location = new System.Drawing.Point(40, 60);
            this.TXB_Fat.Name = "TXB_Fat";
            this.TXB_Fat.Size = new System.Drawing.Size(100, 26);
            this.TXB_Fat.TabIndex = 2;
            this.TXB_Fat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXB_Fat_KeyPress);
            // 
            // TXB_Cal
            // 
            this.TXB_Cal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Cal.Location = new System.Drawing.Point(392, 60);
            this.TXB_Cal.Name = "TXB_Cal";
            this.TXB_Cal.Size = new System.Drawing.Size(100, 26);
            this.TXB_Cal.TabIndex = 3;
            this.TXB_Cal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXB_Cal_KeyPress);
            // 
            // BTN_Fat
            // 
            this.BTN_Fat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Fat.Location = new System.Drawing.Point(40, 108);
            this.BTN_Fat.Name = "BTN_Fat";
            this.BTN_Fat.Size = new System.Drawing.Size(100, 25);
            this.BTN_Fat.TabIndex = 4;
            this.BTN_Fat.Text = "CALCULATE";
            this.BTN_Fat.UseVisualStyleBackColor = true;
            this.BTN_Fat.Click += new System.EventHandler(this.BTN_Fat_Click);
            // 
            // BTN_Cal
            // 
            this.BTN_Cal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Cal.Location = new System.Drawing.Point(392, 110);
            this.BTN_Cal.Name = "BTN_Cal";
            this.BTN_Cal.Size = new System.Drawing.Size(100, 23);
            this.BTN_Cal.TabIndex = 5;
            this.BTN_Cal.Text = "CALCULATE";
            this.BTN_Cal.UseVisualStyleBackColor = true;
            this.BTN_Cal.Click += new System.EventHandler(this.BTN_Cal_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(48, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 37);
            this.label3.TabIndex = 6;
            this.label3.Text = "FAT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(380, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 37);
            this.label4.TabIndex = 7;
            this.label4.Text = "CARBS";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "CALORIES:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(388, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 22);
            this.label2.TabIndex = 9;
            this.label2.Text = "CALORIES:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 309);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BTN_Cal);
            this.Controls.Add(this.BTN_Fat);
            this.Controls.Add(this.TXB_Cal);
            this.Controls.Add(this.TXB_Fat);
            this.Controls.Add(this.LBL_Cal);
            this.Controls.Add(this.LBL_Fat);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LBL_Fat;
        private System.Windows.Forms.Label LBL_Cal;
        private System.Windows.Forms.TextBox TXB_Fat;
        private System.Windows.Forms.TextBox TXB_Cal;
        private System.Windows.Forms.Button BTN_Fat;
        private System.Windows.Forms.Button BTN_Cal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

