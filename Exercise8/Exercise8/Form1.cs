﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise8
{
    public partial class Form1 : Form
    {

        int CaloriesFat;
        int CaloriesCarbs;

        public Form1()
        {
            InitializeComponent();
        }

        private void BTN_Fat_Click(object sender, EventArgs e)
        {
            FatCalories(Convert.ToInt32(TXB_Fat.Text));
            LBL_Fat.Text = Convert.ToString(CaloriesFat);
        }

        private void BTN_Cal_Click(object sender, EventArgs e)
        {
            CarbCalories(Convert.ToInt32(TXB_Cal.Text));
            LBL_Cal.Text = Convert.ToString(CaloriesCarbs);
        }

        public void FatCalories(int FatGrams)
        {
            CaloriesFat = FatGrams * 9;
        }

        public void CarbCalories(int CarbGrams)
        {
            CaloriesCarbs = CarbGrams * 4;
        }

        private void TXB_Fat_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void TXB_Cal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
