﻿namespace Exercise4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_Convert = new System.Windows.Forms.Button();
            this.LBL_TimeElapsed = new System.Windows.Forms.Label();
            this.LBL_TimeFrame = new System.Windows.Forms.Label();
            this.TXB_Seconds = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BTN_Convert
            // 
            this.BTN_Convert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Convert.Location = new System.Drawing.Point(161, 115);
            this.BTN_Convert.Name = "BTN_Convert";
            this.BTN_Convert.Size = new System.Drawing.Size(77, 27);
            this.BTN_Convert.TabIndex = 9;
            this.BTN_Convert.Text = "Convert";
            this.BTN_Convert.UseVisualStyleBackColor = true;
            this.BTN_Convert.Click += new System.EventHandler(this.BTN_Convert_Click);
            // 
            // LBL_TimeElapsed
            // 
            this.LBL_TimeElapsed.AutoSize = true;
            this.LBL_TimeElapsed.Enabled = false;
            this.LBL_TimeElapsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_TimeElapsed.Location = new System.Drawing.Point(193, 216);
            this.LBL_TimeElapsed.Name = "LBL_TimeElapsed";
            this.LBL_TimeElapsed.Size = new System.Drawing.Size(0, 22);
            this.LBL_TimeElapsed.TabIndex = 8;
            this.LBL_TimeElapsed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LBL_TimeFrame
            // 
            this.LBL_TimeFrame.AutoSize = true;
            this.LBL_TimeFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_TimeFrame.Location = new System.Drawing.Point(95, 177);
            this.LBL_TimeFrame.Name = "LBL_TimeFrame";
            this.LBL_TimeFrame.Size = new System.Drawing.Size(120, 22);
            this.LBL_TimeFrame.TabIndex = 7;
            this.LBL_TimeFrame.Text = "Showing in :";
            // 
            // TXB_Seconds
            // 
            this.TXB_Seconds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Seconds.Location = new System.Drawing.Point(131, 72);
            this.TXB_Seconds.Name = "TXB_Seconds";
            this.TXB_Seconds.Size = new System.Drawing.Size(134, 26);
            this.TXB_Seconds.TabIndex = 6;
            this.TXB_Seconds.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXB_Seconds_KeyPress);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(32, 35);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(347, 22);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Please Enter an Ammount of Seconds";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 305);
            this.Controls.Add(this.BTN_Convert);
            this.Controls.Add(this.LBL_TimeElapsed);
            this.Controls.Add(this.LBL_TimeFrame);
            this.Controls.Add(this.TXB_Seconds);
            this.Controls.Add(this.Label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BTN_Convert;
        internal System.Windows.Forms.Label LBL_TimeElapsed;
        internal System.Windows.Forms.Label LBL_TimeFrame;
        internal System.Windows.Forms.TextBox TXB_Seconds;
        internal System.Windows.Forms.Label Label1;
    }
}

