﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TXB_Seconds_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void BTN_Convert_Click(object sender, EventArgs e)
        {
            double seconds = Convert.ToDouble(TXB_Seconds.Text);
            if (seconds >= 60 && seconds < 3600)
            {
                TimeSpan time = TimeSpan.FromSeconds(seconds);
                String str = time.ToString(@"mm");
                LBL_TimeFrame.Text = "Showing in: Minutes";
                LBL_TimeElapsed.Text = str;

            }else if(seconds >= 3600 && seconds < 86400)
            {
                TimeSpan time = TimeSpan.FromSeconds(seconds);
                String str = time.ToString(@"hh");
                LBL_TimeFrame.Text = "Showing in: Hours";
                LBL_TimeElapsed.Text = str;

            }
            else if(seconds >= 86400)
            {
                TimeSpan time = TimeSpan.FromSeconds(seconds);
                String str = time.ToString(@"dd");
                LBL_TimeFrame.Text = "Showing in: Days";
                LBL_TimeElapsed.Text = str;

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
