﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginScreen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BTN_Login = New System.Windows.Forms.Button()
        Me.LBL_Username = New System.Windows.Forms.Label()
        Me.LBL_Password = New System.Windows.Forms.Label()
        Me.TXB_Username = New System.Windows.Forms.TextBox()
        Me.TXB_Password = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'BTN_Login
        '
        Me.BTN_Login.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_Login.Location = New System.Drawing.Point(429, 240)
        Me.BTN_Login.Name = "BTN_Login"
        Me.BTN_Login.Size = New System.Drawing.Size(101, 33)
        Me.BTN_Login.TabIndex = 0
        Me.BTN_Login.Text = "Login"
        Me.BTN_Login.UseVisualStyleBackColor = True
        '
        'LBL_Username
        '
        Me.LBL_Username.AutoSize = True
        Me.LBL_Username.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_Username.Location = New System.Drawing.Point(105, 168)
        Me.LBL_Username.Name = "LBL_Username"
        Me.LBL_Username.Size = New System.Drawing.Size(183, 25)
        Me.LBL_Username.TabIndex = 1
        Me.LBL_Username.Text = "Username or Email:"
        '
        'LBL_Password
        '
        Me.LBL_Password.AutoSize = True
        Me.LBL_Password.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_Password.Location = New System.Drawing.Point(184, 207)
        Me.LBL_Password.Name = "LBL_Password"
        Me.LBL_Password.Size = New System.Drawing.Size(104, 25)
        Me.LBL_Password.TabIndex = 2
        Me.LBL_Password.Text = "Password:"
        '
        'TXB_Username
        '
        Me.TXB_Username.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXB_Username.Location = New System.Drawing.Point(316, 165)
        Me.TXB_Username.Name = "TXB_Username"
        Me.TXB_Username.Size = New System.Drawing.Size(214, 30)
        Me.TXB_Username.TabIndex = 3
        Me.TXB_Username.Text = "Username"
        '
        'TXB_Password
        '
        Me.TXB_Password.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TXB_Password.Location = New System.Drawing.Point(316, 204)
        Me.TXB_Password.Name = "TXB_Password"
        Me.TXB_Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TXB_Password.Size = New System.Drawing.Size(214, 30)
        Me.TXB_Password.TabIndex = 4
        Me.TXB_Password.Text = "Password"
        Me.TXB_Password.UseSystemPasswordChar = True
        '
        'LoginScreen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(729, 500)
        Me.Controls.Add(Me.TXB_Password)
        Me.Controls.Add(Me.TXB_Username)
        Me.Controls.Add(Me.LBL_Password)
        Me.Controls.Add(Me.LBL_Username)
        Me.Controls.Add(Me.BTN_Login)
        Me.Name = "LoginScreen"
        Me.Text = "Login"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BTN_Login As Button
    Friend WithEvents LBL_Username As Label
    Friend WithEvents LBL_Password As Label
    Friend WithEvents TXB_Username As TextBox
    Friend WithEvents TXB_Password As TextBox
End Class
