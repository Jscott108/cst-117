﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Runtime.InteropServices.ComTypes;
using System.Text.RegularExpressions;

namespace Exercise10
{
    class Program
    {

        static void Main(string[] args)
        {
            //Main getting a file path
            Console.WriteLine("Please Enter a filepath for the text file");
            string filepath = Console.ReadLine();
            readWords(filepath);
        }

        public static void readWords(string file)
        {

            int counter = 0;
            //getting each word from the file
            string[] text = System.IO.File.ReadAllText(file).Split(' ');
            foreach (string word in text)
            {
                
                //Removing any special characters from the word
                string finalString = Regex.Replace(word, @"[^0-9a-zA-Z]+", "");

                //Getting the last character in the word
                int length = finalString.Length;
                char lastLetter = finalString[length - 1];

                //checking if the letter is 'e' ot 't'
                if(lastLetter == 't' || lastLetter == 'e')
                {
                    counter += 1;
                }
            }

            //Display the output
            Console.WriteLine("There are " + counter + " words that end in t or e");
            
            //wait for input
            Console.WriteLine("Press any Key to continue");
            Console.ReadKey();
            
            //loop
            restart();
        }

        
        //Copy of main() to act as a loop
        static void restart()
        {
            Console.WriteLine("Please Enter a filepath for the text file");
            string filepath = Console.ReadLine();
            readWords(filepath);
        }
    }
}
