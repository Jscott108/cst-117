﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConversionCalculator
{



    public partial class MMtoINconversion : Form
    {
        Decimal Millimeters;
        Decimal Centemeters;
        Decimal Inches;
        Decimal CmToIn;
        Boolean Letters;

        public MMtoINconversion()
        {
            InitializeComponent();

        }

        private void BTN_Convert_click(object sender, EventArgs e)
        {
            IsDigitsOnly(TXB_Millimeters.Text);

            if (Letters == true) {
                MessageBox.Show("Please use only numbers", "Numbers only!!!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else {
                CmToIn = (decimal)(2.54);
                Millimeters = Convert.ToDecimal(TXB_Millimeters.Text);
                if (Millimeters == 0)
                {
                    MessageBox.Show("Error, dividing by 0 will casue total event colapse... please reconsider.", "Divide by 0 Detected",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Centemeters = Millimeters / 10;
                    Inches = Centemeters / CmToIn;
                    Inches = Math.Truncate(Inches * 1000) / 1000;
                    TXB_Inches.Text = Convert.ToString(Inches);
                }
            }
        }

        private void TXB_Millimeters_TextChanged(object sender, EventArgs e)
        {

        }
        
        public void IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (!Char.IsLetter(c))
                {
                    Letters = false;
                }
                else
                {
                    Letters = true;
                    return;
                }
            }
        }
    }
}
