﻿namespace Exercise6
{
    partial class DiceRoller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTN_Roll = new System.Windows.Forms.Button();
            this.LBL_Dice1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LBL_Dice2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BTN_Roll
            // 
            this.BTN_Roll.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Roll.Location = new System.Drawing.Point(12, 64);
            this.BTN_Roll.Name = "BTN_Roll";
            this.BTN_Roll.Size = new System.Drawing.Size(115, 115);
            this.BTN_Roll.TabIndex = 0;
            this.BTN_Roll.Text = "Roll the Dice";
            this.BTN_Roll.UseVisualStyleBackColor = true;
            this.BTN_Roll.Click += new System.EventHandler(this.BTN_Roll_Click);
            // 
            // LBL_Dice1
            // 
            this.LBL_Dice1.AutoSize = true;
            this.LBL_Dice1.BackColor = System.Drawing.SystemColors.ControlText;
            this.LBL_Dice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Dice1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LBL_Dice1.Location = new System.Drawing.Point(199, 87);
            this.LBL_Dice1.Name = "LBL_Dice1";
            this.LBL_Dice1.Size = new System.Drawing.Size(0, 73);
            this.LBL_Dice1.TabIndex = 3;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox2.Location = new System.Drawing.Point(329, 64);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(115, 115);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(166, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(115, 115);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // LBL_Dice2
            // 
            this.LBL_Dice2.AutoSize = true;
            this.LBL_Dice2.BackColor = System.Drawing.SystemColors.ControlText;
            this.LBL_Dice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Dice2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LBL_Dice2.Location = new System.Drawing.Point(355, 87);
            this.LBL_Dice2.Name = "LBL_Dice2";
            this.LBL_Dice2.Size = new System.Drawing.Size(0, 73);
            this.LBL_Dice2.TabIndex = 4;
            // 
            // DiceRoller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(456, 260);
            this.Controls.Add(this.LBL_Dice2);
            this.Controls.Add(this.LBL_Dice1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BTN_Roll);
            this.Name = "DiceRoller";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTN_Roll;
        private System.Windows.Forms.Label LBL_Dice1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LBL_Dice2;
    }
}

