﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise6
{
    public partial class DiceRoller : Form
    {
        private int Dice = 6;
        public int Roll1;
        public int Roll2;
        public int TimesRolled = 0;
        public Boolean CurrentlyRolling = false;
        public DiceRoller()
        {
            InitializeComponent();
        }

        private void BTN_Roll_Click(object sender, EventArgs e)
        {
            if (CurrentlyRolling == false)
            {
                CurrentlyRolling = true;
                BTN_Roll.Text = "Rolling...";
                rollDie();
            }
            
        }
    
        async private void rollDie()
        {
            Boolean SnakeEyes = false;
            Random rnd = new Random();
            TimesRolled = 0;

            while (SnakeEyes == false)
            {
                Roll1 = rnd.Next(1, Dice + 1);
                Roll2 = rnd.Next(1, Dice + 1);
                TimesRolled += 1;

                LBL_Dice1.Text = Convert.ToString(Roll1);
                LBL_Dice2.Text = Convert.ToString(Roll2);

                if (Roll1 == 1 && Roll2 == 1)
                {
                    SnakeEyes = true;
                }
                await Task.Delay(1000);
            }

            System.Windows.Forms.MessageBox.Show("It took " + TimesRolled + " rolls to get Snake Eyes");
            CurrentlyRolling = false;
            BTN_Roll.Text = "Roll the Dice";
        }

    }
}
