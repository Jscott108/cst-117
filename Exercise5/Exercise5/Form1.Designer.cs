﻿namespace Exercise5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.TXB_Terms = new System.Windows.Forms.TextBox();
            this.BTN_Calculate = new System.Windows.Forms.Button();
            this.LBL_Terms = new System.Windows.Forms.Label();
            this.LBL_PI = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(50, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter # of terms: ";
            // 
            // TXB_Terms
            // 
            this.TXB_Terms.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Terms.Location = new System.Drawing.Point(221, 84);
            this.TXB_Terms.Name = "TXB_Terms";
            this.TXB_Terms.Size = new System.Drawing.Size(193, 27);
            this.TXB_Terms.TabIndex = 1;
            this.TXB_Terms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXB_Terms_KeyPress);
            // 
            // BTN_Calculate
            // 
            this.BTN_Calculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Calculate.Location = new System.Drawing.Point(114, 152);
            this.BTN_Calculate.Name = "BTN_Calculate";
            this.BTN_Calculate.Size = new System.Drawing.Size(101, 34);
            this.BTN_Calculate.TabIndex = 2;
            this.BTN_Calculate.Text = "Calculate";
            this.BTN_Calculate.UseVisualStyleBackColor = true;
            this.BTN_Calculate.Click += new System.EventHandler(this.BTN_Calculate_Click);
            // 
            // LBL_Terms
            // 
            this.LBL_Terms.AutoSize = true;
            this.LBL_Terms.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Terms.Location = new System.Drawing.Point(54, 218);
            this.LBL_Terms.Name = "LBL_Terms";
            this.LBL_Terms.Size = new System.Drawing.Size(370, 22);
            this.LBL_Terms.TabIndex = 3;
            this.LBL_Terms.Text = "Approximate value of PI after xxxx terms";
            // 
            // LBL_PI
            // 
            this.LBL_PI.AutoSize = true;
            this.LBL_PI.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_PI.Location = new System.Drawing.Point(54, 278);
            this.LBL_PI.Name = "LBL_PI";
            this.LBL_PI.Size = new System.Drawing.Size(22, 22);
            this.LBL_PI.TabIndex = 4;
            this.LBL_PI.Text = "=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 450);
            this.Controls.Add(this.LBL_PI);
            this.Controls.Add(this.LBL_Terms);
            this.Controls.Add(this.BTN_Calculate);
            this.Controls.Add(this.TXB_Terms);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "App-roximate PI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXB_Terms;
        private System.Windows.Forms.Button BTN_Calculate;
        private System.Windows.Forms.Label LBL_Terms;
        private System.Windows.Forms.Label LBL_PI;
    }
}

