﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BTN_Calculate_Click(object sender, EventArgs e)
        {
            double terms = Convert.ToDouble(TXB_Terms.Text);
            double PiOver = 0;
            double sign = 1;
            
            for(int term = 0; term < terms; term++)
            {
                Console.WriteLine(sign + " / " + (term * 2 + 1) + " = " +
                (1.0 / (term * 2 + 1)));
                PiOver += sign / (term * 2 + 1);
                sign *= -1;
            }

            double pi = 4 * PiOver;
            LBL_PI.Text = "= " + pi.ToString();
            LBL_Terms.Text = "Approximate value of PI after " + Convert.ToString(terms) + "terms";




        }

        private void TXB_Terms_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
