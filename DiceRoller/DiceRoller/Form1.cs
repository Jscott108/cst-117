﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiceRoller
{
    public partial class Form1 : Form
    {
        int DiceToRoll;
        int Roll;
        int Total = 0;
        

        public Form1()
        {
            InitializeComponent();
        }

        private void DiceType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BTN_Roll_Click(object sender, EventArgs e)
        {
            String Dice = Convert.ToString(DiceType.GetItemText(DiceType.SelectedItem));
            bool MultiDice = false;
            bool Nat20 = false;
            GetDiceToRoll(Dice);
            int Bonuses;
            int Proficiency;
            int Multi;
            Total = 0;
            Roll = 0;

            //Check how many dice need to be rolled
            if (RDO_Multi.Checked)
            {
                MultiDice = true;
                //check if a number had been entered
                if (TXB_RollMultiplyer.Text.Length == 0)
                {
                    Multi = 1;
                }
                else
                {
                    Multi = Convert.ToInt32(TXB_RollMultiplyer.Text);
                }

                for (int i = 1; i <= Multi; i++)
                {

                    Random r = new Random();
                    Roll = r.Next(1, DiceToRoll + 1);
                    Total = Roll + Total;
                    Roll = Total;
                }
            }
            else
            {
                Random r = new Random();
                Roll = r.Next(1, DiceToRoll + 1);
                Total = Roll;
            }

            //Add Bonuses
            if (CHB_Bonuse.Checked)
            {
                //check if a number had been entered
                if (TXB_Bonus.Text.Length == 0)
                {
                    Bonuses = 0;
                }
                else
                {
                    Bonuses = Convert.ToInt32(TXB_Bonus.Text);
                }
                Total = Total + Bonuses;
            }

            //Add Proficiency
            if (CHB_Proficiency.Checked)
            {
                //check if a number had been entered
                if (TXB_Proficiency.Text.Length == 0)
                {
                    Proficiency = 0;
                }
                else
                { 
                    Proficiency = Convert.ToInt32(TXB_Proficiency.Text);
                }
                Total = Total + Proficiency;
            }

            //Check if it is a NAT 20
            if (Roll == 20 && Dice.Equals("D-20") && MultiDice == false)
            {
                Nat20 = true;
            }

            //Display results
            if (Nat20 == true)
            {
                LBL_NatRoll.Text = "NATURAL 20!!!";
                LBL_TotalRoll.Text = Convert.ToString(Total);

                System.Media.SoundPlayer Nat20Sound = new System.Media.SoundPlayer(Properties.Resources.NAT20);
                Nat20Sound.Play();
            }
            else
            {
                LBL_NatRoll.Text = Convert.ToString(Roll);
                LBL_TotalRoll.Text = Convert.ToString(Total);
            }
        }


        //Determing the type of dice to roll
        private void GetDiceToRoll(String DieType)
        {
            if (DieType.Equals("D-4"))
            {
                DiceToRoll = 4;
            }
            else if (DieType.Equals("D-6"))
            {
                DiceToRoll = 6;
            }
            else if (DieType.Equals("D-8"))
            {
                DiceToRoll = 8;
            }
            else if (DieType.Equals("D-10"))
            {
                DiceToRoll = 10;
            }
            else if (DieType.Equals("D-12"))
            {
                DiceToRoll = 12;
            }
            else if (DieType.Equals("D-20"))
            {
                DiceToRoll = 20;
            }
            else if (DieType.Equals("D-100"))
            {
                DiceToRoll = 100;
            }
        }

        //Check only numbers are accepted
        private void TXB_RollMultiplyer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void TXB_Proficiency_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void TXB_Bonus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void DiceType_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void BTN_Clear_Click(object sender, EventArgs e)
        {
            LBL_NatRoll.Text = ("");
            LBL_TotalRoll.Text = ("");
        }
    }
}