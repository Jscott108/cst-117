﻿namespace DiceRoller
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CHB_Proficiency = new System.Windows.Forms.CheckBox();
            this.CHB_Bonuse = new System.Windows.Forms.CheckBox();
            this.RDO_Once = new System.Windows.Forms.RadioButton();
            this.RDO_Multi = new System.Windows.Forms.RadioButton();
            this.TXB_RollMultiplyer = new System.Windows.Forms.TextBox();
            this.TXB_Proficiency = new System.Windows.Forms.TextBox();
            this.TXB_Bonus = new System.Windows.Forms.TextBox();
            this.DiceType = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DiceCountBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BTN_Roll = new System.Windows.Forms.Button();
            this.BTN_Clear = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.LBL_NatRoll = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LBL_TotalRoll = new System.Windows.Forms.Label();
            this.DiceCountBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CHB_Proficiency
            // 
            this.CHB_Proficiency.AutoSize = true;
            this.CHB_Proficiency.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHB_Proficiency.Location = new System.Drawing.Point(6, 25);
            this.CHB_Proficiency.Name = "CHB_Proficiency";
            this.CHB_Proficiency.Size = new System.Drawing.Size(169, 26);
            this.CHB_Proficiency.TabIndex = 0;
            this.CHB_Proficiency.Text = "Add Proficiency";
            this.CHB_Proficiency.UseVisualStyleBackColor = true;
            // 
            // CHB_Bonuse
            // 
            this.CHB_Bonuse.AutoSize = true;
            this.CHB_Bonuse.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHB_Bonuse.Location = new System.Drawing.Point(6, 60);
            this.CHB_Bonuse.Name = "CHB_Bonuse";
            this.CHB_Bonuse.Size = new System.Drawing.Size(182, 26);
            this.CHB_Bonuse.TabIndex = 1;
            this.CHB_Bonuse.Text = "Add Other Bonus";
            this.CHB_Bonuse.UseVisualStyleBackColor = true;
            // 
            // RDO_Once
            // 
            this.RDO_Once.AutoSize = true;
            this.RDO_Once.Checked = true;
            this.RDO_Once.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RDO_Once.Location = new System.Drawing.Point(6, 19);
            this.RDO_Once.Name = "RDO_Once";
            this.RDO_Once.Size = new System.Drawing.Size(116, 26);
            this.RDO_Once.TabIndex = 2;
            this.RDO_Once.TabStop = true;
            this.RDO_Once.Text = "Roll Once";
            this.RDO_Once.UseVisualStyleBackColor = true;
            // 
            // RDO_Multi
            // 
            this.RDO_Multi.AutoSize = true;
            this.RDO_Multi.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RDO_Multi.Location = new System.Drawing.Point(6, 51);
            this.RDO_Multi.Name = "RDO_Multi";
            this.RDO_Multi.Size = new System.Drawing.Size(138, 26);
            this.RDO_Multi.TabIndex = 3;
            this.RDO_Multi.Text = "Roll Multiple";
            this.RDO_Multi.UseVisualStyleBackColor = true;
            // 
            // TXB_RollMultiplyer
            // 
            this.TXB_RollMultiplyer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_RollMultiplyer.Location = new System.Drawing.Point(149, 50);
            this.TXB_RollMultiplyer.Name = "TXB_RollMultiplyer";
            this.TXB_RollMultiplyer.Size = new System.Drawing.Size(100, 27);
            this.TXB_RollMultiplyer.TabIndex = 4;
            this.TXB_RollMultiplyer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXB_RollMultiplyer_KeyPress);
            // 
            // TXB_Proficiency
            // 
            this.TXB_Proficiency.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Proficiency.Location = new System.Drawing.Point(194, 24);
            this.TXB_Proficiency.Name = "TXB_Proficiency";
            this.TXB_Proficiency.Size = new System.Drawing.Size(100, 27);
            this.TXB_Proficiency.TabIndex = 5;
            this.TXB_Proficiency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXB_Proficiency_KeyPress);
            // 
            // TXB_Bonus
            // 
            this.TXB_Bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXB_Bonus.Location = new System.Drawing.Point(194, 59);
            this.TXB_Bonus.Name = "TXB_Bonus";
            this.TXB_Bonus.Size = new System.Drawing.Size(100, 27);
            this.TXB_Bonus.TabIndex = 6;
            this.TXB_Bonus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXB_Bonus_KeyPress);
            // 
            // DiceType
            // 
            this.DiceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiceType.FormattingEnabled = true;
            this.DiceType.ItemHeight = 20;
            this.DiceType.Items.AddRange(new object[] {
            "D-4",
            "D-6",
            "D-8",
            "D-10",
            "D-12",
            "D-20",
            "D-100"});
            this.DiceType.Location = new System.Drawing.Point(28, 34);
            this.DiceType.Name = "DiceType";
            this.DiceType.Size = new System.Drawing.Size(138, 144);
            this.DiceType.TabIndex = 7;
            this.DiceType.SelectedIndexChanged += new System.EventHandler(this.DiceType_SelectedIndexChanged);
            this.DiceType.SelectedValueChanged += new System.EventHandler(this.DiceType_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "Select Dice";
            // 
            // DiceCountBox
            // 
            this.DiceCountBox.Controls.Add(this.RDO_Once);
            this.DiceCountBox.Controls.Add(this.RDO_Multi);
            this.DiceCountBox.Controls.Add(this.TXB_RollMultiplyer);
            this.DiceCountBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiceCountBox.Location = new System.Drawing.Point(28, 222);
            this.DiceCountBox.Name = "DiceCountBox";
            this.DiceCountBox.Size = new System.Drawing.Size(255, 100);
            this.DiceCountBox.TabIndex = 9;
            this.DiceCountBox.TabStop = false;
            this.DiceCountBox.Text = "Dice Rolled";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CHB_Proficiency);
            this.groupBox1.Controls.Add(this.CHB_Bonuse);
            this.groupBox1.Controls.Add(this.TXB_Bonus);
            this.groupBox1.Controls.Add(this.TXB_Proficiency);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(28, 328);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 100);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bonuses";
            // 
            // BTN_Roll
            // 
            this.BTN_Roll.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Roll.Location = new System.Drawing.Point(420, 264);
            this.BTN_Roll.Name = "BTN_Roll";
            this.BTN_Roll.Size = new System.Drawing.Size(75, 35);
            this.BTN_Roll.TabIndex = 11;
            this.BTN_Roll.Text = "Roll";
            this.BTN_Roll.UseVisualStyleBackColor = true;
            this.BTN_Roll.Click += new System.EventHandler(this.BTN_Roll_Click);
            // 
            // BTN_Clear
            // 
            this.BTN_Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Clear.Location = new System.Drawing.Point(420, 305);
            this.BTN_Clear.Name = "BTN_Clear";
            this.BTN_Clear.Size = new System.Drawing.Size(75, 37);
            this.BTN_Clear.TabIndex = 12;
            this.BTN_Clear.Text = "Clear";
            this.BTN_Clear.UseVisualStyleBackColor = true;
            this.BTN_Clear.Click += new System.EventHandler(this.BTN_Clear_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(202, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 26);
            this.label2.TabIndex = 14;
            this.label2.Text = "Natural Roll:";
            // 
            // LBL_NatRoll
            // 
            this.LBL_NatRoll.AutoSize = true;
            this.LBL_NatRoll.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_NatRoll.Location = new System.Drawing.Point(340, 49);
            this.LBL_NatRoll.Name = "LBL_NatRoll";
            this.LBL_NatRoll.Size = new System.Drawing.Size(29, 31);
            this.LBL_NatRoll.TabIndex = 15;
            this.LBL_NatRoll.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(202, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(206, 26);
            this.label4.TabIndex = 16;
            this.label4.Text = "Total With Bonuses:";
            // 
            // LBL_TotalRoll
            // 
            this.LBL_TotalRoll.AutoSize = true;
            this.LBL_TotalRoll.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_TotalRoll.Location = new System.Drawing.Point(414, 106);
            this.LBL_TotalRoll.Name = "LBL_TotalRoll";
            this.LBL_TotalRoll.Size = new System.Drawing.Size(29, 31);
            this.LBL_TotalRoll.TabIndex = 17;
            this.LBL_TotalRoll.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 450);
            this.Controls.Add(this.LBL_TotalRoll);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LBL_NatRoll);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BTN_Clear);
            this.Controls.Add(this.BTN_Roll);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.DiceCountBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DiceType);
            this.Name = "Form1";
            this.Text = "Form1";
            this.DiceCountBox.ResumeLayout(false);
            this.DiceCountBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox CHB_Proficiency;
        private System.Windows.Forms.CheckBox CHB_Bonuse;
        private System.Windows.Forms.RadioButton RDO_Once;
        private System.Windows.Forms.RadioButton RDO_Multi;
        private System.Windows.Forms.TextBox TXB_RollMultiplyer;
        private System.Windows.Forms.TextBox TXB_Proficiency;
        private System.Windows.Forms.TextBox TXB_Bonus;
        private System.Windows.Forms.ListBox DiceType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox DiceCountBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BTN_Roll;
        private System.Windows.Forms.Button BTN_Clear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LBL_NatRoll;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LBL_TotalRoll;
    }
}

